let http = require("http");
const PORT = 3000;

let directory = [
{
	"name": "Brandon",
	"email": "brand@gmail.com"
},
{
	"name": "Robert",
	"email": "robert@gmail.com"
}
]


http.createServer( (req,res) => {
	console.log(req);
	if(req.url === "/users" && req.method === "GET"){
		res.writeHead(200, {"Content-Type": "application/json"});
		res.write(JSON.stringify(directory));
		res.end();
	}
	else if(req.url === "/users" && req.method === "POST"){
		let reqBody = "";
		req.on("data", (data) => {
			reqBody += data;
			// console.log(data);

		} );
		req.on("end", () => {

			reqBody = JSON.parse(reqBody);
			console.log(reqBody);

			let newUser = {
				"name": reqBody.name,
				"email": reqBody.email
			}

			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, {"Content-Type" : "application/json"});
			res.write(JSON.stringify(directory));
			res.end();

		});
		/*res.writeHead(200, {"Content-Type": "application/json"});
		res.write(JSON.stringify(directory));
		res.end();*/
	}
	



}).listen(PORT);

console.log(`Server is now connected to the post ${PORT}`)